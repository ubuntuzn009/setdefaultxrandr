#include <stdio.h>  
#include <stdlib.h>  
#include <string.h>  
#include <glib/gi18n.h>
#include <X11/Xatom.h>
#include <math.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "savexrandr.h"
#include "edid.h"

#define portnum	6
static const struct {
  char name[10], path[50];
}port_info[] = {
	{"DP1", "/sys/class/drm/card0-DP-1"},
	{"DP2", "/sys/class/drm/card0-DP-2"},
	{"VGA1", "/sys/class/drm/card0-VGA-1"},
	{"HDMI1", "/sys/class/drm/card0-HDMI-A-1"},
	{"HDMI2", "/sys/class/drm/card0-HDMI-A-2"},
	{"eDP1", "/sys/class/drm/card0-eDP-1"},
};

int decode_resolution_by_file (const char *filename, xRandrCurrentMonitor *monitor);

static void
xrandrcurrentmonitor_init(xRandrCurrentMonitor *monitor)
{
	int i;
	memset(monitor->manufacturer_code, 0, sizeof(monitor->manufacturer_code));
	monitor->product_code = -1;
	memset(monitor->port, 0, sizeof(monitor->port));
	for(i = 0; i < 32; i++)
	{
		monitor->modes[i].width = 0;
		monitor->modes[i].height = 0;
		monitor->modes[i].frequency = 0;
	}
	//monitor->modes = NULL;
	monitor->nmode = 0;
}

xRandrCurrentMonitor *
xrandrcurrentmonitor_new()
{
	xRandrCurrentMonitor *monitor;

	monitor = g_slice_new0 (xRandrCurrentMonitor);
	xrandrcurrentmonitor_init(monitor);

	return monitor;
}

xRandrCurrent *
xRandrCurrent_new()
{
	xRandrCurrent *current;

	current = g_slice_new0 (xRandrCurrent);
	current->count = 0;
	current->monitor1 = xrandrcurrentmonitor_new();
	current->monitor2 = xrandrcurrentmonitor_new();
	current->monitor3 = xrandrcurrentmonitor_new();

	return current;
}

static int 
xRandrCurrentMonitor_get_monitor(xRandrCurrentMonitor	*monitor, 
							char *edidpath)
{
	unsigned char *edid_data;
	int fd;
	int success = 0;

	fd = open(edidpath, O_RDONLY);
	if(fd == -1)
	{
		return 0;
	}
	edid_data = extract_edid(fd);
	close(fd);

	if(edid_data)
	{
		success = decode_edid2 (edid_data, monitor);
		g_free (edid_data);
	}

	return success;
}

xRandrCurrent *
xRandrCurrent_get_current_monitor()
{
	xRandrCurrent *current;
	xRandrCurrentMonitor	*monitor = NULL;
	FILE *fp = NULL;
	int i;
	char statuspath[100], edidpath[100], filebuf[100];

	current = xRandrCurrent_new();
	for (i = 0; i < portnum; i++)
	{
		if (strcmp(current->monitor1->manufacturer_code, "") == 0)
		{
			monitor = current->monitor1;
		}
		else if (strcmp(current->monitor2->manufacturer_code, "") == 0)
		{
			monitor = current->monitor2;
		}	
		else if (strcmp(current->monitor3->manufacturer_code, "") == 0)
		{
			monitor = current->monitor3;
		}
		else
		{
			return current;
		}
		memset(statuspath, 0, sizeof(statuspath));
		snprintf(statuspath, sizeof(statuspath), "%s/status", port_info[i].path);
		fp = fopen (statuspath, "r");
		if(fp == NULL)
		{
			continue;
		}
		memset(filebuf, 0, sizeof(filebuf));
		fgets(filebuf, sizeof(filebuf), fp);
		fclose(fp);
		fp = NULL;
		filebuf[strlen(filebuf) - 1] = '\0';
		if(strcmp(filebuf, "connected") == 0)
		{
			strncpy(monitor->port, port_info[i].name, sizeof(monitor->port));

			memset(edidpath, 0, sizeof(edidpath));
			snprintf(edidpath, sizeof(edidpath), "%s/edid", port_info[i].path);
			if(xRandrCurrentMonitor_get_monitor(monitor, edidpath) == 0)
			{
				xrandrcurrentmonitor_init(monitor);
			}
			else
			{
				decode_resolution_by_file (port_info[i].path, monitor);
				current->count++;
			}
		}
	}//FOR
	return current;
}

void
xrandrcurrent_free(xRandrCurrent *current)
{
	if(current)
	{
		current->count = 0;
		if(current->monitor1)
		{
			g_slice_free (xRandrCurrentMonitor, current->monitor1);
		}
		if(current->monitor2)
		{
			g_slice_free (xRandrCurrentMonitor, current->monitor2);
		}
		if(current->monitor3)
		{
			g_slice_free (xRandrCurrentMonitor, current->monitor3);
		}
		g_slice_free (xRandrCurrent, current);
	}
}

xRandrRecordMonitor *
xrandrrecordmonitor_new()
{
	xRandrRecordMonitor *monitor;

	monitor = g_slice_new0 (xRandrRecordMonitor);
	memset(monitor->manufacturer_code, 0, sizeof(monitor->manufacturer_code));
	monitor->product_code = -1;
	memset(monitor->port, 0, sizeof(monitor->port));
	monitor->enable = TRUE;
	memset(monitor->mode, 0, sizeof(monitor->mode));
	monitor->primary = FALSE;
	memset(monitor->rotation, 0, sizeof(monitor->rotation));

	return monitor;
}

xRandrRecord *
xrandrrecord_new()
{
	xRandrRecord *record;

	record = g_slice_new0 (xRandrRecord);
	record->count = 0;
	record->monitor1 = xrandrrecordmonitor_new();
	record->monitor2 = xrandrrecordmonitor_new();
	record->monitor3 = xrandrrecordmonitor_new();
	record->pNext = NULL;

	return record;
}

static void 
xrandrrecordmonitor_copy(xRandrRecordMonitor *dest, xRandrRecordMonitor *src)
{
	if( !dest || !src)
	{
		return;
	}
	
	strncpy(dest->manufacturer_code, src->manufacturer_code, sizeof(dest->manufacturer_code));	
	dest->product_code = src->product_code;
	strncpy(dest->port, src->port, sizeof(dest->port));
	dest->enable = src->enable;
	strncpy(dest->mode, src->mode, sizeof(dest->mode));
	dest->primary = src->primary;
	strncpy(dest->rotation, src->rotation, sizeof(dest->rotation));
}

static void
xrandrrecord_add(xRandrRecord *list, xRandrRecord *node)
{
	if(list == NULL || node == NULL)
	{
		return;
	}

	if(list->count == 0)
	{
		list->count = node->count;
		if(node->monitor1 != NULL)
		{
			list->monitor1 = xrandrrecordmonitor_new();
			xrandrrecordmonitor_copy(list->monitor1, node->monitor1);
		}
		if(node->monitor2 != NULL)
		{
			list->monitor2 = xrandrrecordmonitor_new();
			xrandrrecordmonitor_copy(list->monitor2, node->monitor2);
		}
		if(node->monitor3 != NULL)
		{
			list->monitor3 = xrandrrecordmonitor_new();
			xrandrrecordmonitor_copy(list->monitor3, node->monitor3);
		}
		xrandrrecord_free(node);

		return;
	}
	
	xRandrRecord *tmp = list;
	while(tmp->pNext != NULL)
	{
		tmp = tmp->pNext;
	}
	tmp->pNext = node;
	
	return;
}

void
xrandrrecord_free(xRandrRecord *record)
{
	xRandrRecord *r1;

	while(record)
	{
		r1 = record;
		record = record->pNext;
		r1->count = 0;
		if(r1->monitor1)
		{
			g_slice_free (xRandrRecordMonitor, r1->monitor1);
			r1->monitor1 = NULL;
		}
		if(r1->monitor2)
		{
			g_slice_free (xRandrRecordMonitor, r1->monitor2);
			r1->monitor2 = NULL;
		}
		if(r1->monitor3)
		{
			g_slice_free (xRandrRecordMonitor, r1->monitor3);
			r1->monitor3 = NULL;
		}
		g_slice_free (xRandrRecord, r1);
	}
}

xRandrCmd *
xRandrCmd_new()
{
	xRandrCmd *Rcmd;

	Rcmd = g_slice_new0 (xRandrCmd);
	memset(Rcmd->primon, 0, sizeof(Rcmd->primon));
	memset(Rcmd->secmon1, 0, sizeof(Rcmd->secmon1));
	memset(Rcmd->secmon2, 0, sizeof(Rcmd->secmon2));
	memset(Rcmd->offmon1, 0, sizeof(Rcmd->offmon1));
	memset(Rcmd->offmon2, 0, sizeof(Rcmd->offmon2));
	memset(Rcmd->prires, 0, sizeof(Rcmd->prires));
	memset(Rcmd->secres1, 0, sizeof(Rcmd->secres1));
	memset(Rcmd->secres2, 0, sizeof(Rcmd->secres2));
	memset(Rcmd->prirot, 0, sizeof(Rcmd->prirot));
	memset(Rcmd->secrot1, 0, sizeof(Rcmd->secrot1));
	memset(Rcmd->secrot2, 0, sizeof(Rcmd->secrot2));

	return Rcmd;
}

int
xRandrCmd_init(xRandrCmd *Rcmd, xRandrRecordMonitor *monitor)
{
	if(strcmp(monitor->manufacturer_code, "") != 0)
	{
		if(monitor->enable == 0)
		{
			if(strcmp(Rcmd->offmon1, "") == 0)
			{
				strncpy(Rcmd->offmon1, monitor->port, sizeof(Rcmd->offmon1));
			}
			else if(strcmp(Rcmd->offmon2, "") == 0)
			{
				strncpy(Rcmd->offmon2, monitor->port, sizeof(Rcmd->offmon2));
			}
			else
			{
				printf("err!\n");
				return 0;
			}
		}
		else if(monitor->enable == 1)
		{
			if(monitor->primary == 1)
			{
				strncpy(Rcmd->primon, monitor->port, sizeof(Rcmd->primon));
				strncpy(Rcmd->prires, monitor->mode, sizeof(Rcmd->prires));
				strncpy(Rcmd->prirot, monitor->rotation, sizeof(Rcmd->prirot));
			}
			else
			{
				if(strcmp(Rcmd->secmon1, "") == 0)
				{
					strncpy(Rcmd->secmon1, monitor->port, sizeof(Rcmd->secmon1));
					strncpy(Rcmd->secres1, monitor->mode, sizeof(Rcmd->secres1));
					strncpy(Rcmd->secrot1, monitor->rotation, sizeof(Rcmd->secrot1));
				}
				else if(strcmp(Rcmd->secmon2, "") == 0)
				{
					strncpy(Rcmd->secmon2, monitor->port, sizeof(Rcmd->secmon2));
					strncpy(Rcmd->secres2, monitor->mode, sizeof(Rcmd->secres2));
					strncpy(Rcmd->secrot2, monitor->rotation, sizeof(Rcmd->secrot2));
				}
				else
				{
					printf("err!\n");
					return 0;
				}
			}
		}
		else
		{
			printf("err!\n");
			return 0;
		}
	}
	return 1;
}

void
xRandrCmd_free(xRandrCmd *Rcmd)
{
	g_slice_free (xRandrCmd, Rcmd);
}

void
get_suitable_resolution(xRandrCurrentMonitor *monitor, 
					char *mode)
{
	int i;

	for(i = 0; i < monitor->nmode; i++)
	{
		if (i == 0 
		|| (i > 0 && (monitor->modes[i].width != monitor->modes[i - 1].width
		|| monitor->modes[i].height != monitor->modes[i - 1].height)))
		{
			if(strstr(monitor->port, "VGA") != NULL)
			{
				if( monitor->modes[i].width > VGAMAXWIDTH 
				|| monitor->modes[i].height > VGAMAXHEIGHT)
				{
					continue;
				} 
				
			}
			else if(strstr(monitor->port, "HDMI") != NULL)
			{
				if( monitor->modes[i].width > HDMIMAXWIDTH 
				|| monitor->modes[i].height > HDMIMAXHEIGHT)
				{
					continue;
				} 
			}
			else if(strstr(monitor->port, "DP") != NULL)
			{
				if( monitor->modes[i].width > DPMAXWIDTH 
				|| monitor->modes[i].height > DPMAXHEIGHT)
				{
					continue;
				} 
			}
			sprintf(mode, "%dx%d", monitor->modes[i].width, monitor->modes[i].height);
			printf("mode:%dx%d\n", monitor->modes[i].width, monitor->modes[i].height);
			break;
		}
	}
}

static int
match_current_record_monitor(xRandrRecordMonitor *recmonitor, 
						xRandrCurrentMonitor *curmonitor) 
{
	int match = 0;

	if(   strcmp(recmonitor->port, curmonitor->port) == 0
	&& strcmp(recmonitor->manufacturer_code, curmonitor->manufacturer_code) == 0
	&& recmonitor->product_code == curmonitor->product_code)
	{
		match = 1;
		/*  厂商，产品代码，接口都相同表示为同一显示器，不再判断分辨率
		int i;
		char mode[20];

		for(i = 0; i < curmonitor->nmode; i++)
		{
			if (i == 0 
			|| (i > 0 && (curmonitor->modes[i].width != curmonitor->modes[i - 1].width
			|| curmonitor->modes[i].height != curmonitor->modes[i - 1].height)))
			{
				memset(mode, 0, sizeof(mode));
				sprintf(mode, "%dx%d", curmonitor->modes[i].width, curmonitor->modes[i].height);
				if(strcmp(mode, recmonitor->mode) == 0)
				{
					match = 1;
					break;
				}
			}
		}
		*/
	}
	return match;
}

xRandrRecord *
match_current_record(xRandrRecord *record, xRandrCurrent *current)
{
	xRandrRecord *recordnode = record;

	while(recordnode)
	{
		if(recordnode->count != current->count)
		{//printf("count no match!\n");
			recordnode = recordnode->pNext;
			continue;
		}
		if(recordnode->count == 2)
		{
			if(   match_current_record_monitor(recordnode->monitor1, current->monitor1) == 1
			&& match_current_record_monitor(recordnode->monitor2, current->monitor2) == 1)
			{
				return recordnode;
			}
			else if( match_current_record_monitor(recordnode->monitor1, current->monitor2) == 1
				&&match_current_record_monitor(recordnode->monitor2, current->monitor1) == 1)
			{
				return recordnode;
			}
		}
		else if(recordnode->count == 1)
		{
			if(match_current_record_monitor(recordnode->monitor1, current->monitor1) == 1)
			{
				return recordnode;
			}
		}
		else if(recordnode->count == 3)
		{
			if(match_current_record_monitor(recordnode->monitor1, current->monitor1) == 1)
			{
				if( (match_current_record_monitor(recordnode->monitor2, current->monitor2) == 1
				&&match_current_record_monitor(recordnode->monitor3, current->monitor3) == 1)
				|| (match_current_record_monitor(recordnode->monitor2, current->monitor3) == 1
				&&match_current_record_monitor(recordnode->monitor3, current->monitor2) == 1) )
				{
					return recordnode;
				}
			}
			else if(match_current_record_monitor(recordnode->monitor1, current->monitor2) == 1)
			{
				if( (match_current_record_monitor(recordnode->monitor2, current->monitor1) == 1
				&&match_current_record_monitor(recordnode->monitor3, current->monitor3) == 1)
				|| (match_current_record_monitor(recordnode->monitor2, current->monitor3) == 1
				&&match_current_record_monitor(recordnode->monitor3, current->monitor1) == 1) )
				{
					return recordnode;
				}
			}
			else if(match_current_record_monitor(recordnode->monitor1, current->monitor3) == 1)
			{
				if( (match_current_record_monitor(recordnode->monitor2, current->monitor1) == 1
				&&match_current_record_monitor(recordnode->monitor3, current->monitor2) == 1)
				|| (match_current_record_monitor(recordnode->monitor2, current->monitor2) == 1
				&&match_current_record_monitor(recordnode->monitor3, current->monitor1) == 1) )
				{
					return recordnode;
				}
			}
		}
		//printf("data no match!\n");
		recordnode = recordnode->pNext;
	}
	return NULL;
}

void
xrandr_record_print(xRandrRecord *record)
{
	xRandrRecord *tmprecord = record;
	while(tmprecord)
	{
		if(strcmp(tmprecord->monitor1->manufacturer_code, "") != 0)
		{
			printf("monitor1:----------------------\n");
			printf("manufacturer_code:%s\n", tmprecord->monitor1->manufacturer_code);
			printf("product_code:%d\n", tmprecord->monitor1->product_code);
			printf("port:%s\n", tmprecord->monitor1->port);
			printf("enable:%d\n", tmprecord->monitor1->enable);
			printf("mode:%s\n", tmprecord->monitor1->mode);
			printf("primary:%d\n", tmprecord->monitor1->primary);
			printf("rotation:%s\n", tmprecord->monitor1->rotation);
		}
		if(strcmp(tmprecord->monitor2->manufacturer_code, "") != 0)
		{
			printf("monitor2:----------------------\n");
			printf("manufacturer_code:%s\n", tmprecord->monitor2->manufacturer_code);
			printf("product_code:%d\n", tmprecord->monitor2->product_code);
			printf("port:%s\n", tmprecord->monitor2->port);
			printf("enable:%d\n", tmprecord->monitor2->enable);
			printf("mode:%s\n", tmprecord->monitor2->mode);
			printf("primary:%d\n", tmprecord->monitor2->primary);
			printf("rotation:%s\n", tmprecord->monitor2->rotation);
		}
		if(strcmp(tmprecord->monitor3->manufacturer_code, "") != 0)
		{
			printf("monitor3:----------------------\n");
			printf("manufacturer_code:%s\n", tmprecord->monitor3->manufacturer_code);
			printf("product_code:%d\n", tmprecord->monitor3->product_code);
			printf("port:%s\n", tmprecord->monitor3->port);
			printf("enable:%d\n", tmprecord->monitor3->enable);
			printf("mode:%s\n", tmprecord->monitor3->mode);
			printf("primary:%d\n", tmprecord->monitor3->primary);
			printf("rotation:%s\n", tmprecord->monitor3->rotation);
		}
		tmprecord = tmprecord->pNext;
	}
}

void
xrandr_current_monitor_print(xRandrCurrentMonitor *monitor)
{
	if(monitor)
	{
		printf("manufacturer_code:%s\n", monitor->manufacturer_code);
		printf("product_code:%d\n", monitor->product_code);
		printf("port:%s\n", monitor->port);
		int i;
		for(i = 0; i < monitor->nmode; i++)
		{
			if (i == 0 || (i > 0 && (monitor->modes[i].width != monitor->modes[i - 1].width
		    || monitor->modes[i].height != monitor->modes[i - 1].height)))
			{
				printf("mode:%dx%d\n", monitor->modes[i].width, monitor->modes[i].height);
			}
		}
	}
}

static void 
xrandr_get_record_monitor_info(GKeyFile* kf, 
						char *grp, 
						int num, 
						xRandrRecordMonitor *monitor)
{
	char keyname[20];
	gchar *str;
	 
	memset(keyname, 0, sizeof(keyname));
	sprintf(keyname, "ManufacturerCode%d", num);
	str = g_key_file_get_string(kf, grp, keyname, NULL);
	if(str != NULL)
	{
		strncpy(monitor->manufacturer_code, str, sizeof(monitor->manufacturer_code));
	}
	else
	{
		return;
	}

	memset(keyname, 0, sizeof(keyname));
	sprintf(keyname, "ProductCode%d", num);
	monitor->product_code = g_key_file_get_integer(kf, grp, keyname, NULL);

	memset(keyname, 0, sizeof(keyname));
	sprintf(keyname, "Port%d", num);
	str = g_key_file_get_string(kf, grp, keyname, NULL);
	if(str != NULL)
	{
		strncpy(monitor->port, str, sizeof(monitor->port));
	}
	else
	{
		return;
	}

	memset(keyname, 0, sizeof(keyname));
	sprintf(keyname, "Enable%d", num);
	monitor->enable = g_key_file_get_boolean(kf, grp, keyname, NULL);

	memset(keyname, 0, sizeof(keyname));
	sprintf(keyname, "Mode%d", num);
	str = g_key_file_get_string(kf, grp, keyname, NULL);
	if(str != NULL)
	{
		strncpy(monitor->mode, str, sizeof(monitor->mode));
	}
	else
	{
		return;
	}

	memset(keyname, 0, sizeof(keyname));
	sprintf(keyname, "Primary%d", num);
	monitor->primary = g_key_file_get_boolean(kf, grp, keyname, NULL);

	memset(keyname, 0, sizeof(keyname));
	sprintf(keyname, "Rotation%d", num);
	str = g_key_file_get_string(kf, grp, keyname, NULL);
	if(str != NULL)
	{
		strncpy(monitor->rotation, str, sizeof(monitor->rotation));
	}
	else
	{
		return;
	}
}

int
xrandr_read_record(char *userhomepath, 
				xRandrRecord *record)
{
	char grp[10];
    	GKeyFile* kf;
	char filename[100]; 
	int irecord = 0, count, i;

	kf = g_key_file_new();
	memset(filename, 0, sizeof(filename));
	snprintf(filename, sizeof(filename), "%s/.config/xrandrrecord.ini", userhomepath);

	if(TRUE == g_key_file_load_from_file(kf, filename, G_KEY_FILE_NONE, NULL))
	{
		irecord = 1;
		while(1)
		{
			memset(grp, 0, sizeof(grp));
			snprintf(grp, sizeof(grp), "RECORD%d", irecord);
			count = g_key_file_get_integer(kf, grp, "DisplayNum", NULL);
			if(count != 1 && count != 2 && count != 3)
			{
				break;
			}
			xRandrRecord *tmprecord = NULL;
			tmprecord = xrandrrecord_new();
			tmprecord->count = count;
			for(i = 0; i < count; i++)
			{
				if(strcmp(tmprecord->monitor1->manufacturer_code, "") == 0)
				{
					xrandr_get_record_monitor_info(kf, grp, i+1, tmprecord->monitor1);
				}
				else if(strcmp(tmprecord->monitor2->manufacturer_code, "") == 0)
				{
					xrandr_get_record_monitor_info(kf, grp, i+1, tmprecord->monitor2);					
				}
				else
				{
					xrandr_get_record_monitor_info(kf, grp, i+1, tmprecord->monitor3);
				}
			}
			xrandrrecord_add(record, tmprecord);
			irecord++;
		}
	}

	if(kf)
		g_key_file_free(kf);
	if(irecord > 0)
		return 1;
	else
		return 0;
}

void
parse_mode(char *mode, char *w, char *h)
{
	char *modetmp = mode;
	char *p, *ps;

	p = strtok_r(modetmp, "x", &ps);
	if(p)
	{
		strcpy(w, p);
	}
	p = strtok_r(NULL, "x", &ps);
	if(p)
	{
		strcpy(h, p);
	}
}

int
decode_resolution_by_file (const char *filename, xRandrCurrentMonitor *monitor)
{
	int i, idx;
	char cmd[1024];
	char output[1024], wstr[10], hstr[10];
	FILE *fp = NULL;

	idx = 0;
	memset(cmd, 0, sizeof(cmd));
	memset(output, 0, sizeof(output));
	snprintf(cmd, sizeof(cmd), "cat %s/modes | grep -v i | uniq", filename);
	
	if(strcmp(cmd, "") != 0)
	{
		fp = popen(cmd, "r");
		if(fp == NULL)
		{ 
			return 0;
		}
		while(fgets(output, sizeof(output), fp))
		{
			if(strstr(output, "x") != NULL)
			{
				sscanf(output, "%s\n", output);
				memset(wstr, 0, sizeof(wstr));
				memset(hstr, 0, sizeof(hstr));
				parse_mode(output, wstr, hstr);
				monitor->modes[idx].width = atoi(wstr);
				monitor->modes[idx].height = atoi(hstr);
				monitor->modes[idx].frequency = 60;
				idx++;	
			}
			memset(output, 0, sizeof(output));
		}
		pclose(fp);
	}
	monitor->nmode = idx;
	/*for(i=0; i<monitor->nmode; i++)
	{
		printf("%dx%d@%d\n", monitor->modes[i].width, monitor->modes[i].height, monitor->modes[i].frequency);
	}*/	

	return idx;
}
