#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <glib/gi18n.h>

#define SHPATH			"/.config/newvirtualres.sh"
#define AUOSTARTPATH	"/.config/autostart/lxrandr-autostart.desktop"
#define USERCOUNTS		10

#define STARTUPLOCALUSERPATH	"/usr/local/share/lightdm/lightdm.conf.d/lightdm.conf"
#define STARTUPUSERPATH			"/etc/lightdm/lightdm.conf"


typedef struct monitor_information
{
	char monitor[10];
	char res[20];
	char allres[1024];
	int priority;//1:VGA 2:HDMI 3:DP
}MonInfo;

