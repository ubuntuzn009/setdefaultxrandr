
#ifndef SAVE_XRADNR_H
#define SAVE_XRADNR_H

#include "edid.h"

#define VGAMAXWIDTH		1920
#define VGAMAXHEIGHT		1280
#define HDMIMAXWIDTH		1920
#define HDMIMAXHEIGHT		1280
#define DPMAXWIDTH			2560
#define DPMAXHEIGHT		1600

typedef struct _xRandrRecord 	xRandrRecord;
typedef struct _xRandrRecordMonitor	xRandrRecordMonitor;
typedef struct _xRandrCmd	xRandrCmd;


struct _xRandrRecord
{
	int					count;
	xRandrRecordMonitor	*monitor1;
	xRandrRecordMonitor	*monitor2;
	xRandrRecordMonitor	*monitor3;
	struct _xRandrRecord	*pNext;
};
struct _xRandrRecordMonitor
{
	char	manufacturer_code[4];
    	int		product_code;
	char 	port[7];
	int		enable;
	char	mode[10];
	int		primary;
	char	rotation[10];	
};
struct _xRandrCmd
{
	char	primon[7];
	char	secmon1[7];
	char	secmon2[7];
	char	offmon1[7];
	char	offmon2[7];
	char	prires[10];
	char	secres1[10];
	char	secres2[10];
	char	prirot[10];
	char	secrot1[10];
	char	secrot2[10];
};

int
xrandr_read_record(char *userconfigpath, 
				xRandrRecord *record);

xRandrCurrent *
xRandrCurrent_new();

void
xrandrcurrent_free(xRandrCurrent *current);

void
xrandr_current_monitor_print(xRandrCurrentMonitor *monitor);

xRandrRecord *
xrandrrecord_new();

void
xrandrrecord_free(xRandrRecord *record);

xRandrCmd *
xRandrCmd_new();

int
xRandrCmd_init(xRandrCmd *Rcmd, xRandrRecordMonitor *monitor);

void
xRandrCmd_free(xRandrCmd *Rcmd);

void
xrandr_record_print(xRandrRecord *record);

xRandrRecord *
match_current_record(xRandrRecord *record, 
				xRandrCurrent *current);

void
xrandr_make_command(xRandrRecord *record, 
					GString *cmd);

void
get_suitable_resolution(xRandrCurrentMonitor *monitor, 
					char *mode);
xRandrCurrent *
xRandrCurrent_get_current_monitor();

#endif
