/* edid.h
 *
 * Copyright 2007, 2008, Red Hat, Inc.
 *
 * This file is part of the Gnome Library.
 *
 * The Gnome Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Gnome Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the Gnome Library; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Soren Sandmann <sandmann@redhat.com>
 */

/* Downloaded from <http://git.gnome.org/browse/gnome-desktop/tree/libgnome-desktop>
   (git commit 42452cada8cf1c4d7a81aded0a3ddbb5e97441de) */

#ifndef EDID_H
#define EDID_H

typedef unsigned char uchar;
typedef struct Timing Timing;
typedef struct _xRandrCurrent	xRandrCurrent;
typedef struct _xRandrCurrentMonitor	xRandrCurrentMonitor;

struct Timing
{
    int width;
    int height;
    int frequency;
};

struct _xRandrCurrent
{
	int					count;
	xRandrCurrentMonitor	*monitor1;
	xRandrCurrentMonitor	*monitor2;
	xRandrCurrentMonitor	*monitor3;
};

struct _xRandrCurrentMonitor
{
	char			manufacturer_code[4];
    	int				product_code;
	char 			port[7];
	Timing			modes[32];
	int 				nmode;
};

int decode_edid2 (const unsigned char *edid, xRandrCurrentMonitor *monitor);
unsigned char *extract_edid(int fd);


#endif
